<?php

namespace App\Service;

use App\Entity\Booking;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Dompdf\Dompdf;
use Dompdf\Options;
use Twig\Environment;
use Twilio\Rest\Client;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 9/2/2020
 * Time: 1:17 PM
 */
class GeneralService
{
    private $passwordEncoder;

    private $security;

    public $entityManager;

    private $twig;

    public $seesion;

    private $parameterBag;

    /**
     * GeneralService constructor.
     * @param $passwordEncoder UserPasswordEncoderInterface
     * @param $security Security
     * @param $entityManager EntityManagerInterface
     * @param $twig Environment
     * @param $session SessionInterface
     * @param $parameterBag ParameterBagInterface
     */
    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        Security $security,
        EntityManagerInterface $entityManager,
        Environment $twig,
        SessionInterface $session,
        ParameterBagInterface $parameterBag
        )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->security = $security;
        $this->entityManager = $entityManager;
        $this->twig = $twig;
        $this->seesion = $session;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @param $user User
     */
    public function encodePassword($user)
    {
        $user->setPassword($this->passwordEncoder->encodePassword($user,$user->getPassword()));
    }

    public function getUser()
    {
        return $this->security->getUser();
    }

    public function getTodayBookings($today=null)
    {
        if ($today == null){
            $today = new \DateTime('now',new \DateTimeZone('Europe/Bucharest'));
            $today->setTime(0,0);
            $tomorrow = new \DateTime('tomorrow');
        }
        else{
            /**
             * @var \DateTime $tomorrow
             */
            $tomorrow = new \DateTime($today);
            $today = new \DateTime($today);
            $tomorrow->add(new \DateInterval('P1D'));
        }

        $query = $this->entityManager->createQuery('
            SELECT b
            FROM App\Entity\Booking b 
            WHERE b.school = :school AND b.beginAt BETWEEN :start AND :finish
            ORDER BY b.beginAt 
        ')
            ->setParameters([
                'start' => $today,
                'finish' => $tomorrow,
                'school' => $this->getUser()
            ]);
        return $query->getResult();
    }

    public function generateWorksheetPDF()
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->twig->render('school/worksheet_pdf.html.twig', [
           'bookings' => $this->getTodayBookings($_SESSION['today']),
            'day' => $_SESSION['today']
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("worksheet.pdf", [
            "Attachment" => false
        ]);
    }

    public function sendSMS($phone,$message)
    {
        $account_sid = $this->parameterBag->get('twillio_account_id');
        $auth_token = $this->parameterBag->get('twillio_auth_token');
        $twilio_number = $this->parameterBag->get('twillio_number');
        dd($twilio_number);
        $twilio_client = new Client($account_sid, $auth_token);
        $twilio_client->messages->create(
            $phone, [
                'from' => $twilio_number,
                'body' => $message
            ]
        );
    }

    public function notifyClient()
    {
        $today = new \DateTime('now',new \DateTimeZone('Europe/Bucharest'));
        $today->setTime(0,0);
        $tomorrow = new \DateTime('tomorrow');
        $query = $this->entityManager->createQuery('
            SELECT b
            FROM App\Entity\Booking b 
            WHERE b.beginAt BETWEEN :start AND :finish
        ')
            ->setParameters([
                'start' => $today,
                'finish' => $tomorrow
            ]);
        $bookings = $query->getResult();
        /**
         * @var $booking Booking
         */
        foreach ($bookings as $booking){
            $time = $booking->getBeginAt()->format('H:i');
            $message = 'Va amintim ca astazi la '.$time.' aveti programare la '.$booking->getSchool()->getName();
            $this->sendSMS($booking->getClient()->getPhone(),$message);
        }
    }

    public function setAdminRole($name)
    {
        $query = $this->entityManager->createQuery('
            SELECT u
            FROM App\Entity\User u 
            WHERE u.username = :name ')
            ->setParameter('name', $name);
        /**
         * @var $user User[]
         */
        $user = $query->getResult();
        $roles = [];
        $roles[] = 'ROLE_ADMIN';
        $user[0]->setRoles($roles);
        $this->entityManager->flush();
    }

    public function getSchools()
    {
        $role = '[]';
        $query = $this->entityManager->createQuery('
            SELECT u
            FROM App\Entity\User u 
            WHERE u.roles = :val ')
            ->setParameter('val', $role);
        return $query->getResult();
    }

    public function getSchoolBookingsBetweenDates($user,$start,$end)
    {
        $query = $this->entityManager->createQuery('
            SELECT b
            FROM App\Entity\Booking b
            WHERE b.school = :school 
            AND b.beginAt BETWEEN :start AND :end 
            ORDER BY b.beginAt')
            ->setParameters([
                'school' => $user,
                'start' => $start,
                'end' => $end
            ]);
        return $query->getResult();
    }

    /**
     * @param $booking Booking
     */
    public function setClassEndTime($booking)
    {
        $classType = $booking->getType();
        $endTime = new \DateTime($booking->getBeginAt()->format('Y-m-d\TH:i'));
        $endTime->add(new \DateInterval('PT'.$classType->getMinutes().'M'));
        $booking->setEndAt($endTime);
    }

    /**
     * @param $client \App\Entity\Client
     */
    public function setInterviewAndPrivateClassNumber(\App\Entity\Client $client)
    {
        $client->setInterview(3);
        $client->setPrivateLesson(0);
        $client->setDoneLessons(0);
    }

    /**
     * @param $booking Booking
     */
    public function interviewCheck($booking)
    {
        if ($booking->getType()->getType() == 'Interview') {
            $booking->getClient()->setInterview($booking->getClient()->getInterview()-1);
        }
        if ($booking->getType()->getType() == 'Lectie privata') {
            $booking->getClient()->setPrivateLesson($booking->getClient()->getPrivateLesson()-1);
            $booking->getClient()->setDoneLessons($booking->getClient()->getDoneLessons()+1);
        }
    }

    public function createAdmin($username, $password, $name)
    {
        $admin = new User();
        $admin->setName($name);
        $cripted = $this->passwordEncoder->encodePassword($admin,$password);
        $admin->setPassword($cripted);
        $admin->setUsername($username);
        $roladmin = [];
        $roladmin[]= 'ROLE_ADMIN';
        $admin->setRoles($roladmin);
        $this->entityManager->persist($admin);
        $this->entityManager->flush();
    }
}