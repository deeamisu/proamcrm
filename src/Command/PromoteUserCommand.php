<?php

namespace App\Command;

use App\Service\GeneralService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PromoteUserCommand extends Command
{
    protected static $defaultName = 'app:promote-user';

    protected $generalService;

    /**
     * PromoteUserCommand constructor.
     * @param $generalService GeneralService
     */
    public function __construct(GeneralService $generalService)
    {
        parent::__construct();
        $this->generalService = $generalService;
    }


    protected function configure()
    {
        $this
            ->setDescription('Promotes an user to admin')
            ->addArgument('user', InputArgument::REQUIRED, 'User username')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $user = $input->getArgument('user');

        /**
        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }
         */

        $this->generalService->setAdminRole($user);
        $io->success('User promoted.');

        return 0;
    }
}
