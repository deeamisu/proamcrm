<?php

namespace App\Command;

use App\Service\GeneralService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ClientNotificationCommand extends Command
{
    protected static $defaultName = 'app:client-notification';

    protected $generalService;

    /**
     * ClientNotificationCommand constructor.
     * @param $generalService GeneralService
     */
    public function __construct(GeneralService $generalService)
    {
        parent::__construct();
        $this->generalService = $generalService;
    }


    protected function configure()
    {
        $this
            ->setDescription('Notifies clients about today\'s appointments' )
            //->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        /**
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }
        */

        $io->title('Sending messages to clients ...');

        $this->generalService->notifyClient();

        $io->success('Clients have been notified !');

        return 0;
    }
}
