<?php

namespace App\Command;

use App\Service\GeneralService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateAdminCommand extends Command
{
    protected static $defaultName = 'app:create-admin';

    protected $generalService;

    /**
     * CreateAdminCommand constructor.
     * @param $generalService GeneralService
     */
    public function __construct(GeneralService $generalService)
    {
        parent::__construct();
        $this->generalService = $generalService;
    }


    protected function configure()
    {
        $this
            ->setDescription('Creates a new admin')
            ->addArgument('usernasme', InputArgument::REQUIRED, 'Admin username')
            ->addArgument('password', InputArgument::REQUIRED, 'Admin password')
            ->addArgument('name', InputArgument::REQUIRED, 'Admin name')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $argUsername = $input->getArgument('usernasme');
        $argPassword = $input->getArgument('password');
        $argName = $input->getArgument('name');
        $this->generalService->createAdmin($argUsername,$argPassword,$argName);

        $io->success('You have created a new admin .');

        return 0;
    }
}
