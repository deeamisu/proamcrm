<?php

/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 9/21/2020
 * Time: 12:21 AM
 */
namespace App\Event;

use App\Entity\Booking;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Component\Form\Form;

class GroupBookingEvent extends Event
{
    const NAME = 'group_booking_form.rendered';

    protected $booking;

    protected $form;

    /**
     * GroupBookingEvent constructor.
     * @param $booking Booking
     * @param $builder Form
     */
    public function __construct(Booking $booking, Form $form)
    {
        $this->booking = $booking;
        $this->form = $form;
    }

    /**
     * @return Form
     */
    public function getForm()
    {
        return $this->form;
    }
}