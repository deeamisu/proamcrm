<?php

namespace App\EventSubscriber;

use App\Entity\ClassType;
use App\Entity\Client;
use App\Entity\Teacher;
use App\Entity\User;
use App\Repository\ClassTypeRepository;
use App\Repository\ClientRepository;
use App\Repository\TeacherRepository;
use App\Service\GeneralService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Event\GroupBookingEvent;

class FormPreSetSubscriber implements EventSubscriberInterface
{
    private $generalService;

    /**
     * FormPreSetSubscriber constructor.
     * @param $generalService GeneralService
     */
    public function __construct(GeneralService $generalService)
    {
        $this->generalService = $generalService;
    }


    public function onFormEventsPRESETDATA(FormEvent $event)
    {
        /**
         * @var $user User
         */
        $user = $this->generalService->getUser();
        $form = $event->getForm();
        $formOptionsClassType = [
            'class' => ClassType::class,
            'query_builder' => function(ClassTypeRepository $classTypeRepository) {
                return $classTypeRepository->clientClassType();
            }
        ];
        $form->add('type', EntityType::class, $formOptionsClassType);
        $formOptionsTeacher = [
            'class' => Teacher::class,
            'query_builder' => function(TeacherRepository $teacherRepository) use ($user){
                return $teacherRepository->getSchoolTeachers($user);
            }
        ];
        $form->add('teacher', EntityType::class, $formOptionsTeacher);
        $formOptionsClient = [
            'class' => Client::class,
            'query_builder' => function(ClientRepository $clientRepository) use ($user){
                return $clientRepository->getSchoolClients($user);
            }
        ];
        $form->add('client', EntityType::class, $formOptionsClient);
    }

    public function onGroupBooking(GroupBookingEvent $event)
    {
        /**
         * @var $user User
         */
        $user = $this->generalService->getUser();
        $form = $event->getForm();
        $formOptionsClassType = [
            'class' => ClassType::class,
            'query_builder' => function(ClassTypeRepository $classTypeRepository) {
                return $classTypeRepository->groupClassType();
            }
        ];
        $form->add('type', EntityType::class, $formOptionsClassType);
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'onFormEventsPRESETDATA',
            GroupBookingEvent::NAME => 'onGroupBooking',
        ];
    }
}
