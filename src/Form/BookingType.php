<?php

namespace App\Form;

use App\Entity\Booking;
use App\EventSubscriber\FormPreSetSubscriber;
use App\Service\GeneralService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookingType extends AbstractType
{
    private $generalService;

    /**
     * BookingType constructor.
     * @param $generalService GeneralService
     */
    public function __construct(GeneralService $generalService)
    {
        $this->generalService = $generalService;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('beginAt',DateTimeType::class, [
                'widget' => 'single_text'
            ])
            /*->add('endAt', DateTimeType::class,[
                'widget' => 'single_text'
            ])*/
        ;
        $builder->addEventSubscriber(new FormPreSetSubscriber($this->generalService));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
        ]);
    }
}
