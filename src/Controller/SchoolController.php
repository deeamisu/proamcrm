<?php

namespace App\Controller;

use App\Service\GeneralService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;

/**
 *  @IsGranted("ROLE_USER")
 */
class SchoolController extends AbstractController
{
    /**
     * @Route("/school", name="school")
     */
    public function index()
    {
        return $this->render('school/index.html.twig', [
            'controller_name' => 'SchoolController',
        ]);
    }

    /**
     * @Route("/school/worksheet", name="school_worksheet")
     */
    public function worksheet(GeneralService $generalService, Request $request)
    {
        $generalService->seesion->start();
        if ($request->getMethod('POST') && isset($_POST['today'])){
            $_SESSION['today'] = $_POST['today'];
            return $this->render('school/worksheet.html.twig', [
                'bookings' => $generalService->getTodayBookings($_POST['today']),
                'day' => $_POST['today']
            ]);
        }

        $today = new \DateTime('now',new \DateTimeZone('Europe/Bucharest'));
        $_SESSION['today'] = $today->format('Y-m-d');
        return $this->render('school/worksheet.html.twig', [
           'bookings' => $generalService->getTodayBookings(),
           'day' => $today->format('m/d/Y')
        ]);
    }

    /**
     * @Route("/school/worksheet/pdf", name="school_worksheet_pdf")
     */
    public function pdf(GeneralService $generalService)
    {
        $generalService->generateWorksheetPDF();
    }

}
