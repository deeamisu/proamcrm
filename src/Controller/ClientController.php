<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\LessonNumber;
use App\Form\ClientType;
use App\Repository\ClientRepository;
use App\Service\GeneralService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_USER")
 * @Route("/client")
 */
class ClientController extends AbstractController
{
    /**
     * @Route("/", name="client_index", methods={"GET"})
     */
    public function index(ClientRepository $clientRepository, GeneralService $generalService): Response
    {
        return $this->render('client/index.html.twig', [
            //'clients' => $clientRepository->findAll(),
            'clients' => $generalService->getUser()->getClients()
        ]);
    }

    /**
     * @Route("/new", name="client_new", methods={"GET","POST"})
     */
    public function new(Request $request, GeneralService $generalService): Response
    {
        $client = new Client();
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $client->setSchool($generalService->getUser());
            $phone = '+4'.$client->getPhone();
            $client->setPhone($phone);
            $generalService->setInterviewAndPrivateClassNumber($client);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($client);
            $entityManager->flush();

            return $this->redirectToRoute('client_index');
        }

        return $this->render('client/new.html.twig', [
            'client' => $client,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="client_show", methods={"GET"})
     */
    public function show(Client $client): Response
    {
        return $this->render('client/show.html.twig', [
            'client' => $client,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="client_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Client $client): Response
    {
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('client_index');
        }

        return $this->render('client/edit.html.twig', [
            'client' => $client,
            'form' => $form->createView(),
            'lesson_number' => $this->getDoctrine()->getRepository(LessonNumber::class)->findAll()
        ]);
    }

    /**
     * @Route("/{id}", name="client_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Client $client): Response
    {
        if ($this->isCsrfTokenValid('delete'.$client->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($client);
            $entityManager->flush();
        }

        return $this->redirectToRoute('client_index');
    }

    /**
     * @Route("/add/lessons/{id}", name="client_add_lessons", methods={"GET","POST"})
     */
    public function addPrivateClasses($id)
    {
        $lessonsNumber = $_POST['lessonsNumber'];
        unset($_POST['lessonsNumber']);
        /**
         * @var $client Client
         */
        $client = $this->getDoctrine()->getRepository(Client::class)->find($id);
        $client->setPrivateLesson($client->getPrivateLesson() + $lessonsNumber);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('client_show',[
           'id' => $id
        ]);
    }
}
