<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Teacher;
use App\Entity\User;
use App\Service\GeneralService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/schools", name="admin_school_list")
     */
    public function index(GeneralService $generalService)
    {
        return $this->render('admin/index.html.twig', [
            'schools' => $generalService->getSchools(),
        ]);
    }

    /**
     * @Route("/school/{id}", name="admin_school", methods={"GET"})
     */
    public function school($id, GeneralService $generalService)
    {
        $school = $generalService->entityManager->find(User::class,$id);
        return $this->render('admin/school_content.html.twig',[
           'school' => $school
        ]);
    }

    /**
     * @Route("/teacher/{id}", name="admin_school_teacher_bookings", methods={"GET"})
     */
    public function getTeacherBookings($id, GeneralService $generalService)
    {
        $teacher = $generalService->entityManager->find(Teacher::class,$id);
        return $this->render('admin/teacher_bookings.html.twig', [
            'teacher' => $teacher,
        ]);
    }

    /**
     * @Route("/client/{id}", name="admin_school_client_bookings", methods={"GET"})
     */
    public function getClientBookings($id, GeneralService $generalService)
    {
        $client = $generalService->entityManager->find(Client::class,$id);
        return $this->render('admin/client_bookings.html.twig', [
            'client' => $client,
        ]);
    }

    /**
     * @Route("/booking/{id}", name="admin_bookinks", methods={"GET","POST"})
     */
    public function getBookings($id, GeneralService $generalService)
    {
        $start = $_POST['start'];
        $end = $_POST['end'];
        $school = $generalService->entityManager->find(User::class,$id);
        return $this->render('admin/school_bookings.html.twig',[
            'school' => $school,
            'bookings' => $generalService->getSchoolBookingsBetweenDates($school,$start,$end)
        ]);
    }
}
