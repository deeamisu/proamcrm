<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200901185847 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE booking ADD client_id INT DEFAULT NULL, ADD teacher_id INT DEFAULT NULL, ADD school_id INT NOT NULL');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE41807E1D FOREIGN KEY (teacher_id) REFERENCES teacher (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEC32A47EE FOREIGN KEY (school_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_E00CEDDE19EB6921 ON booking (client_id)');
        $this->addSql('CREATE INDEX IDX_E00CEDDE41807E1D ON booking (teacher_id)');
        $this->addSql('CREATE INDEX IDX_E00CEDDEC32A47EE ON booking (school_id)');
        $this->addSql('ALTER TABLE client ADD school_id INT NOT NULL');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455C32A47EE FOREIGN KEY (school_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_C7440455C32A47EE ON client (school_id)');
        $this->addSql('ALTER TABLE teacher ADD school_id INT NOT NULL');
        $this->addSql('ALTER TABLE teacher ADD CONSTRAINT FK_B0F6A6D5C32A47EE FOREIGN KEY (school_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_B0F6A6D5C32A47EE ON teacher (school_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE19EB6921');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE41807E1D');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDEC32A47EE');
        $this->addSql('DROP INDEX IDX_E00CEDDE19EB6921 ON booking');
        $this->addSql('DROP INDEX IDX_E00CEDDE41807E1D ON booking');
        $this->addSql('DROP INDEX IDX_E00CEDDEC32A47EE ON booking');
        $this->addSql('ALTER TABLE booking DROP client_id, DROP teacher_id, DROP school_id');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C7440455C32A47EE');
        $this->addSql('DROP INDEX IDX_C7440455C32A47EE ON client');
        $this->addSql('ALTER TABLE client DROP school_id');
        $this->addSql('ALTER TABLE teacher DROP FOREIGN KEY FK_B0F6A6D5C32A47EE');
        $this->addSql('DROP INDEX IDX_B0F6A6D5C32A47EE ON teacher');
        $this->addSql('ALTER TABLE teacher DROP school_id');
    }
}
